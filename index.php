<?php
if (!empty($_POST)) {
    require("htmlstripapi.php");
    $h = new wits\htmlstripapi;
    $h->run();
} else {
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Test HTMLStripAPI</title>
<style type="text/css">
    * {box-sizing: border-box;}
    body {background-color:#ededed;}
    header {background-color:white;padding:12px 6px; margin-bottom:20px;box-shadow:2px 2px 8px #aaa;}
    header h1 {font:28px Verdana, Sans; margin:10px 0 2px 0;}
    header p {margin:0 0 15px 0; }
    .left {float:left;}
    .right {float:right;}
    .left, .right {box-shadow:2px 2px 8px #aaa;background-color:white;padding:10px;width:48%;}
    form {width:100%;}
    input, textarea {width:100%;padding:6px;background-color:#FFFFF0;}
    textarea {height:150px;}
    textarea#response {min-height:400px;}
    label {display:block;margin-bottom:2px;}
    input[type="button"] {background: #25A6E1;padding:8px 13px;color:#fff;font-size:17px;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;border:1px solid #1A87B9;width:auto;}
    iframe {width:100%;min-height:300px;border:none;background-color:#fffff0;}
</style>
</head>
<body>

<header>
    <h1>HTMLStripAPI</h1>
    <p>This is a test page using HTMLStripAPI to clean up HTML. Most tools like this have a page where you work from, this project can be used as an API with RESTful URLs and POSTing or even GETing data.</p>
</header>

<div class="left">
    <form method="post" id="htmlsource" target="responseiframe">
        <input type="hidden" name="parse" id="parse" value="true" />
        <label for="input">Input HTML</label>
        <textarea name="input" id="input"></textarea>
        <br /><br />
        <label for="ignore">Ignore CSV</label>
        <input type="text" name="ignore" id="ignore" /><br />
        <small>Example: "p,img,h1,h2,h3,a"</small>
        <br /><br />
        <label for="options">Options CSV</label>
        <input type="text" name="options" id="options" /><br />
        <small>ent = convert entities back to characters, i.e. &amp;lt; to <<br />
        nl2br = replaces newlines with &lt;br /&gt;. This is in addition to whether you choose to ignore brs or not.<br />
        nbsp = removes all &amp;nbsp; spaces. Will leave one space after a period if necessary.
        </small>
        <br /><br />
        <input type="submit" value="Submit" />
<!--         <input type="button" value="Send" onclick="loadXMLDoc();" /> -->
    </form>
</div>

<div class="right">
    <label for="response">Response</label>
    <iframe name="responseiframe" src="about:blank"</iframe>
</div>
</body>
</html>
<?php } ?>