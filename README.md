# README #

This is a simple API that accepts RAW HTML and then strips out tags and sends it back as plain text.

## Why? ##

Because I can.
And I wasn't able to find an HTML stripper app that worked via REST API.

## How? ##

If you install this on your server, you can POST your data to it and then use the returned text however you like. There is an included test.html file for figuring things out.