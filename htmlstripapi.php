<?php
namespace wits;

/**
 * An API for processing raw HTML and stripping out tags.
 * 
 * The API is accessible through GET or POST and returns the cleaned HTML as plain text.
 * Various options can be set for ignoring tags, and different character replacements.
 * The code to use is simply this:
 *     require("htmlstripapi.php");
 *     $h = new wits\htmlstripapi;
 *     $h->run();
 * That's it! the API, of course, expects some GET or POST data to process. The accepted variables are listed below.
 *
 * POST or GET to domain.com with the following paramters:
 * @var  input Your original HTML source.
 * @var  ignore A CSV of tags to ignore, ex. "div,p,img,a".
 * @var  options A CSV of available options, ex. "nl2br,ent".
 * @var  parse Do set "parse=true" if you want the API to function.
 *
 * @author  Zack Wallace <zack@wallaceitsolutions.com>
 *
 * @version 0.1b
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU GPL
 * 
 */
class htmlstripapi {

    /** @var string Plain text returned to user */
    private $return  = "";

    /** @var string Original HTML from user */
    private $input   = "";

    /** @var array User-defined tags to ignore, parsed into an array */
    private $ignore  = array();

    /** @var array User-defined options, parsed into an array */
    private $options = array();


    /**
     * The single entry point of the API.
     *
     * Run this method to do it all!
     *
     * This method simply sets the Content-Type and echoes the final parsed HTML in plain text.
     * 
     * @return none This method echos the final string as text/plain.
     */
    public function run() {

        header("Content-Type: text/plain");
        echo $this->return;
    } //run


    /**
     * Run all the needed methods in series as soon as the class is instantiated.
     */
    public function __construct() {
       
        $this->getVars();
        $this->stripHTML();
        $this->runOptions();
    } //construct


    /**
     * Gathers the POST or GET variables and parses the ignore and options into arrays. Sets class properties
     * 
     * @return none
     */
    private function getVars() {

        if(isset($_POST['parse']) && $_POST['parse'] == "true") {

            // They POSTed to the API
            $this->input = urldecode($_POST['input']);
            $ignore = filter_input(INPUT_POST, 'ignore');
            $options = filter_input(INPUT_POST, 'options');
        }

        if(isset($_GET['parse']) && $_GET['parse'] == "true") {

            // They GETed to the API
            $this->input = urldecode($_POST['input']);
            $ignore = filter_input(INPUT_GET, 'ignore');
            $options = filter_input(INPUT_GET, 'options');
        }

        if (!empty($ignore)) {
            // If I don't test for an empty string this returns array([0]=>"") which messes things up
            $this->ignore = str_getcsv($ignore, ",");
        }

        if (!empty($options)) {
            // If I don't test for an empty string this returns array([0]=>"") which messes things up
            $this->options = str_getcsv($options, ",");
        }
    } //getVars


    /**
     * Strips out all HTML tags except those in the ignore array.
     *
     * Uses regular expressions. This methods needs some work on complicated
     * tag structures like <script>. You can ignore <script> but it will still
     * strip all the HTML tags that it finds within the <script>.
     * Once done, it sets the class property.
     *
     * @return none
     */
    private function stripHTML() {

            /** @var string List of tags to ignore, parsed into regex string */
            $tags_exclude = "";

            if(empty($this->ignore)) {
                $regex = "/(<([^>]+)>)/";
            } else {
                foreach ($this->ignore as $k=>$v) {
                    if(preg_match('/^[a-zA-Z]+[a-zA-Z0-9]*$/', $v)) $tags_exclude .= $k==0 ? $v : '|' . $v;
                }
                $regex = "/<(?!\s*\/?(".$tags_exclude.")\b)[^>]+>/";
            }

            $this->return = preg_replace($regex, "", $this->input);
    } //stripHTML


    /**
     * Process the HTML further, based on user options and set the class property.
     *
     * At this point, the primary parsing has already taken place,
     * these optional filters are run after those replacements.
     * 
     * @return none
     */
    private function runOptions() {

        if(in_array("ent", $this->options)) {
            // Convert entities back to real characters.
            // Includes &amp;, &quot;, &#039;, &lt;, &gt;.
            // ENT_QUOTES is set so that it converts both single and double quotes.
            $this->return = htmlspecialchars_decode($this->return, ENT_QUOTES);
        }
        
        if(in_array("nl2br", $this->options)) {
            // If new lines are created, they will be converted to <br />.
            // The user does not have to first ignore BR, as this will add them back in if they don't.
            $this->return = nl2br($this->return);
        }
        
        if(in_array("nbsp", $this->options)) {
            // Removes any &nbsp; spaces.
            // If it immediately comes after a period, we gracious leave one space in there.
            /** TODO: This regex could better detect multiple spaces and gaps and leave appropriate spaces. */
            $this->return = preg_replace(array("/(\.&nbsp;)/", "/(&nbsp;)/"), array(". ", ""), $this->return);
        }

        /** TODO: Sanitize option to remove all properties from ignored tags, i.e. remove style="" from <p> etc. */
        /** TODO: newlines option to remove all but one newline, to avoid having tons of vertical whitespace. */
        /** TODO: comment option to remove html comments and everything between them. This gets rid of a lot of MS junk. */
    } //runOptions

} //htmlstripapi class